package net.therap.meal.system.controller;

import net.therap.meal.system.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Controller
public class AuthController {

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@Param("username") String username,
                          @Param("password") String password,
                          HttpSession session) {

        if (authService.isAuthorized(username, password)) {
            session.setAttribute("user", username);
            return "redirect:/home";
        }

        return "login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        if (session != null) {
            session.removeAttribute("user");
            session.invalidate();
        }
        return "redirect:/login";
    }
}
