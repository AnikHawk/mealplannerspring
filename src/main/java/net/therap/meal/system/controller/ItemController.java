package net.therap.meal.system.controller;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.propertyEditor.ItemPropertyEditor;
import net.therap.meal.system.service.ItemService;
import net.therap.meal.system.validator.ItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */
@Controller
public class ItemController {

    @Autowired
    ItemService itemService;

    @Autowired
    private ItemValidator itemValidator;

    @Autowired
    private ItemPropertyEditor itemPropertyEditor;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(itemValidator);
        binder.registerCustomEditor(Item.class, itemPropertyEditor);
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String getItems(Model model) {
        List<Item> itemList = itemService.findAll();
        model.addAttribute("itemList", itemList);
        return "showItems";
    }

    @RequestMapping(value = "/items/add", method = RequestMethod.GET)
    public String showAddItemPage(Model model) {
        model.addAttribute("item", new Item(""));
        return "addItem";
    }

    @RequestMapping(value = "/items/add", method = RequestMethod.POST)
    public String addItem(@Validated Item item, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "addItem";
        }
        itemService.add(item);
        return "redirect:/items";
    }

    @RequestMapping(value = "/items/update/{id}", method = RequestMethod.GET)
    public ModelAndView showUpdatePage(@PathVariable("id") Item item) {
        ModelAndView mv = new ModelAndView("updateItem");
        if (item == null) {
            return new ModelAndView("error");
        }
        mv.addObject("item", item);
        return mv;
    }

    @RequestMapping(value = "/items/update/{id}", method = RequestMethod.POST)
    public String updateItem(@PathVariable("id") Item item,
                             @Validated @ModelAttribute Item itemAfter,
                             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "updateItem";
        }
        if (item == null) {
            return "error";
        }
        itemService.update(item, itemAfter);
        return "redirect:/items";
    }

    @RequestMapping(value = "/items/delete/{id}", method = RequestMethod.POST)
    public String deleteItem(@PathVariable("id") Item item) {
        itemService.delete(item);
        return "redirect:/items";
    }
}
