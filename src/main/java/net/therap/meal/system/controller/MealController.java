package net.therap.meal.system.controller;

import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.propertyEditor.MealPropertyEditor;
import net.therap.meal.system.service.MealService;
import net.therap.meal.system.validator.MealValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */
@Controller
public class MealController {

    @Autowired
    MealService mealService;

    @Autowired
    private MealValidator mealValidator;

    @Autowired
    private MealPropertyEditor mealPropertyEditor;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(mealValidator);
        binder.registerCustomEditor(Meal.class, mealPropertyEditor);
    }

    @RequestMapping(value = "/meals", method = RequestMethod.GET)
    public String getMeals(Model model) {
        List<Meal> mealList = mealService.findAll();
        model.addAttribute("mealList", mealList);
        return "showMeals";
    }

    @RequestMapping(value = "/meals/add", method = RequestMethod.GET)
    public String showAddMealPage(Model model) {
        model.addAttribute("meal", new Meal(""));
        return "addMeal";
    }

    @RequestMapping(value = "/meals/add", method = RequestMethod.POST)
    public String addMeal(@Validated Meal meal, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "addMeal";
        }
        mealService.add(meal);
        return "redirect:/meals";
    }

    @RequestMapping(value = "/meals/update/{id}", method = RequestMethod.GET)
    public ModelAndView showUpdatePage(@PathVariable("id") Meal meal) {
        ModelAndView mv = new ModelAndView("updateMeal");
        if (meal == null) {
            return new ModelAndView("error");
        }
        mv.addObject("meal", meal);
        return mv;
    }

    @RequestMapping(value = "/meals/update/{id}", method = RequestMethod.POST)
    public String updateMeal(@PathVariable("id") Meal meal,
                             @Validated @ModelAttribute Meal mealAfter,
                             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "updateMeal";
        }
        if (meal == null) {
            return "error";
        }
        mealService.update(meal, mealAfter);
        return "redirect:/meals";
    }

    @RequestMapping(value = "/meals/delete/{id}", method = RequestMethod.POST)
    public String deleteMeal(@PathVariable("id") Meal meal) {
        mealService.delete(meal);
        return "redirect:/meals";
    }
}
