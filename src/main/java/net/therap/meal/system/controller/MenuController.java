package net.therap.meal.system.controller;

import net.therap.meal.system.entity.Day;
import net.therap.meal.system.entity.Item;
import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.entity.Menu;
import net.therap.meal.system.propertyEditor.DayPropertyEditor;
import net.therap.meal.system.propertyEditor.ItemPropertyEditor;
import net.therap.meal.system.propertyEditor.MealPropertyEditor;
import net.therap.meal.system.propertyEditor.MenuPropertyEditor;
import net.therap.meal.system.service.DayService;
import net.therap.meal.system.service.ItemService;
import net.therap.meal.system.service.MealService;
import net.therap.meal.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */

@Controller
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private DayService dayService;

    @Autowired
    private MealService mealService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private MenuPropertyEditor menuPropertyEditor;

    @Autowired
    private ItemPropertyEditor itemPropertyEditor;

    @Autowired
    private MealPropertyEditor mealPropertyEditor;

    @Autowired
    private DayPropertyEditor dayPropertyEditor;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Menu.class, menuPropertyEditor);
        binder.registerCustomEditor(Meal.class, mealPropertyEditor);
        binder.registerCustomEditor(Day.class, dayPropertyEditor);
        binder.registerCustomEditor(Item.class, itemPropertyEditor);
    }

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String getMenus(Model model) {
        List<Menu> menuList = menuService.findAll();
        model.addAttribute("menuList", menuList);
        return "showMenu";
    }

    @RequestMapping(value = "/menu/add", method = RequestMethod.GET)
    public String showAddMenuPage(Model model) {
        List<Day> dayList = dayService.findAll();
        List<Item> itemList = itemService.findAll();
        List<Meal> mealList = mealService.findAll();

        model.addAttribute("day", new Day());
        model.addAttribute("meal", new Meal());
        model.addAttribute("items", new ArrayList<>());
        model.addAttribute("menu", new Menu());

        model.addAttribute("dayList", dayList);
        model.addAttribute("mealList", mealList);
        model.addAttribute("itemList", itemList);

        return "addMenu";
    }

    @RequestMapping(value = "/menu/add", method = RequestMethod.POST)
    public String addItemInMenu(@Validated @ModelAttribute Menu menu, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addMenu";
        }
        menuService.add(menu);
        return "redirect:/menu";
    }

    @RequestMapping(value = "/menu/update/{id}", method = RequestMethod.GET)
    public ModelAndView showMenuUpdatePage(@PathVariable("id") Menu menu) {
        ModelAndView mv = new ModelAndView("updateMenu");
        if (menu == null) {
            return new ModelAndView("error");
        }
        List<Item> itemList = itemService.findAll();
        mv.addObject("menu", menu);
        mv.addObject("itemList", itemList);
        return mv;
    }

    @RequestMapping(value = "/menu/update/{id}", method = RequestMethod.POST)
    public String updateMenu(@PathVariable("id") Menu menu, @ModelAttribute("menu") Menu menuAfter) {
        if (menu == null) {
            return "error";
        }

        for (Item item : menuAfter.getItems()) {
            System.out.println(item.getId() + ": " + item.getName());
        }
        menuService.update(menu, menuAfter);
        return "redirect:/menu";
    }
}
