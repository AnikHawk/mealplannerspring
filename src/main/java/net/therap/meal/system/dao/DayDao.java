package net.therap.meal.system.dao;

import net.therap.meal.system.entity.Day;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */
@Repository
public class DayDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void add(Day day) {
        em.persist(day);
    }

    @Transactional
    public void update(Day day) {
        em.merge(day);
    }

    @Transactional
    public void delete(Integer id) {
        em.remove(findById(id));
    }

    public Day findById(Integer id) {
        return em.find(Day.class, id);
    }

    public List<Day> findAll() {
        return em.createQuery("SELECT day FROM Day day ORDER BY day.id", Day.class).getResultList();
    }
}
