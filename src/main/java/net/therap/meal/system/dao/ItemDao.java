package net.therap.meal.system.dao;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.entity.Meal;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */
@Repository
public class ItemDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void add(Item item) {
        em.persist(item);
    }

    @Transactional
    public void update(Item item) {
        em.merge(item);
    }

    @Transactional
    public void delete(Item item) {
        em.remove(em.contains(item) ? item : em.merge(item));
    }

    public Item findById(Integer id) {
        return em.find(Item.class, id);
    }

    public List<Item> findAll() {
        return em.createQuery("SELECT item FROM Item item ORDER BY item.name", Item.class).getResultList();
    }
}