package net.therap.meal.system.dao;

import net.therap.meal.system.entity.Meal;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */

@Repository
public class MealDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void add(Meal meal) {
        em.persist(meal);
    }

    @Transactional
    public void update(Meal meal) {
        em.merge(meal);
    }

    @Transactional
    public void delete(Meal meal) {
        em.remove(em.contains(meal) ? meal : em.merge(meal));
    }

    public Meal findById(Integer id) {
        return em.find(Meal.class, id);
    }

    public List<Meal> findAll() {
        return em.createQuery("SELECT meal FROM Meal meal ORDER BY meal.name", Meal.class).getResultList();
    }
}