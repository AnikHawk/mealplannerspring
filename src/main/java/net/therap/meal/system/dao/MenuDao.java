package net.therap.meal.system.dao;

import net.therap.meal.system.entity.Day;
import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.entity.Menu;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author altamas.haque
 * @since 3/9/20
 */
@Repository
public class MenuDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Menu menu) {
        em.merge(menu);
    }

    public Menu findById(Integer id) {
        return em.find(Menu.class, id);
    }

    public List<Menu> findAll() {
        TypedQuery<Menu> query = em.createQuery("SELECT menu FROM Menu menu ORDER BY menu.day.id, menu.meal.name", Menu.class);
        return query.getResultList();
    }

    public Menu findByDayAndMeal(Day day, Meal meal) {
        TypedQuery<Menu> query = em.createQuery("SELECT menu FROM Menu menu WHERE menu.day = :day AND menu.meal = :meal ORDER BY menu.day.id, menu.meal.name", Menu.class);
        query.setParameter("day", day).setParameter("meal", meal);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
