package net.therap.meal.system.entity;

import javax.persistence.*;

/**
 * @author altamas.haque
 * @since 2/20/20
 */
@Entity
@Table(name = "Day")
public class Day {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public Day(String name) {
        this.name = name;
    }

    public Day() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Day)) {
            return false;
        }

        Day day = (Day) obj;
        return day.getId() == getId() && day.getName().equals(getName());
    }
}
