package net.therap.meal.system.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * @author altamas.haque
 * @since 2/19/2020
 */
@Entity
@Table(name = "Item")
public class Item {

    @Id
    @Column(name = "item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "item_name")
    @NotEmpty(message = "Please provide a name")
    private String name;

    public Item(String name) {
        this.name = name;
    }

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        if(id == null){
            return super.hashCode();
        }
        return new Long(id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Item)) {
            return false;
        }

        Item item = (Item) obj;
        return item.getId() == getId() && item.getName().equals(getName());
    }
}
