package net.therap.meal.system.entity;

import javax.persistence.*;

/**
 * @author altamas.haque
 * @since 2/19/2020
 */
@Entity
@Table(name = "Meal")
public class Meal {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public Meal(String name) {
        this.name = name;
    }

    public Meal() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return new Long(id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Meal)) {
            return false;
        }

        Meal meal = (Meal) obj;
        return meal.getId() == getId() && meal.getName().equals(getName());
    }
}
