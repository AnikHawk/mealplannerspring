package net.therap.meal.system.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author altamas.haque
 * @since 2/19/2020
 */
@Entity
@Table(name = "Menu")
public class Menu {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private Day day;

    @OneToOne
    private Meal meal;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Menu_Item", joinColumns = {@JoinColumn(name = "id")},
            inverseJoinColumns = {@JoinColumn(name = "items_id")})
    private Set<Item> items;

    public Menu(Day day, Meal meal, Set<Item> items) {
        this.day = day;
        this.meal = meal;
        this.items = items;
    }

    public Menu() {
        this(null, null, new HashSet<>());
    }

    public Menu(Day day, Meal meal) {
        this(day, meal, new HashSet<>());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "id: " + getId() +
                "\nday: " + getDay().getId() + " - " + getDay().getName() +
                "\nmeal: " + getMeal().getId() + " - " + getMeal().getName();
    }
}
