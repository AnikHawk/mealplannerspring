CREATE TABLE `Day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `Item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

CREATE TABLE `Meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

CREATE TABLE `Menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) DEFAULT NULL,
  `meal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_s00eoma5qnd9nyhj088exgxmf` (`day_id`),
  KEY `FK_ssh676b9uvy7yqldlxr7klycm` (`meal_id`),
  CONSTRAINT `FK_s00eoma5qnd9nyhj088exgxmf` FOREIGN KEY (`day_id`) REFERENCES `Day` (`id`),
  CONSTRAINT `FK_ssh676b9uvy7yqldlxr7klycm` FOREIGN KEY (`meal_id`) REFERENCES `Meal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

CREATE TABLE `Menu_Item` (
  `id` int(11) DEFAULT NULL,
  `items_id` int(11) DEFAULT NULL,
  KEY `menu_idx` (`id`),
  KEY `item_idx` (`items_id`),
  CONSTRAINT `item` FOREIGN KEY (`items_id`) REFERENCES `Item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu` FOREIGN KEY (`id`) REFERENCES `Menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into Day(name) values ('saturday');
insert into Day(name) values ('sunday');
insert into Day(name) values ('monday');
insert into Day(name) values ('tuesday');
insert into Day(name) values ('wednesday');
insert into Day(name) values ('thursday');
insert into Day(name) values ('friday');
