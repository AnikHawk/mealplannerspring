package net.therap.meal.system.propertyEditor;

import net.therap.meal.system.entity.Day;
import net.therap.meal.system.service.DayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Component
public class DayPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private DayService dayService;

    @Override
    public String getAsText() {
        Day day = (Day) getValue();
        return day == null ? "" : day.getName();
    }

    @Override
    public void setAsText(String id) throws IllegalArgumentException {
        if (StringUtils.isEmpty(id)) {
            setValue(null);
        }
        else{
            Day day = dayService.findById(Integer.parseInt(id));
            setValue(day);
        }
    }
}
