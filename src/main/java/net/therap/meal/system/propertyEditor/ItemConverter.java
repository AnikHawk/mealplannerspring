package net.therap.meal.system.propertyEditor;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Component
public class ItemConverter implements Converter<String, Item> {

    @Autowired
    private ItemService itemService;

    @Override
    public Item convert(String source) {
        if (StringUtils.isEmpty(source)) {
            return null;
        } else {
            Item item = itemService.findById(Integer.parseInt(source));
            return item;
        }
    }
}
