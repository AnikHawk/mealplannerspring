package net.therap.meal.system.propertyEditor;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Component
public class ItemPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private ItemService itemService;

    @Override
    public String getAsText() {
        Item item = (Item) getValue();
        return item == null ? "" : item.getName();
    }

    @Override
    public void setAsText(String id) throws IllegalArgumentException {
        if (StringUtils.isEmpty(id)) {
            setValue(null);
        }
        else{
            Item item = itemService.findById(Integer.parseInt(id));
            setValue(item);
        }
    }
}
