package net.therap.meal.system.propertyEditor;

import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Component
public class MealPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private MealService mealService;

    @Override
    public String getAsText() {
        Meal meal = (Meal) getValue();
        return meal == null ? "" : meal.getName();
    }

    @Override
    public void setAsText(String id) throws IllegalArgumentException {
        if (StringUtils.isEmpty(id)) {
            setValue(null);
        }
        else{
            Meal meal = mealService.findById(Integer.parseInt(id));
            setValue(meal);
        }
    }
}
