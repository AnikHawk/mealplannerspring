package net.therap.meal.system.propertyEditor;

import net.therap.meal.system.entity.Menu;
import net.therap.meal.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author altamas.haque
 * @since 3/11/20
 */
@Component
public class MenuPropertyEditor extends PropertyEditorSupport {

    @Autowired
    private MenuService menuService;

    @Override
    public String getAsText() {
        Menu menu = (Menu) getValue();
        return menu == null ? "" : menu.getDay().getName() + " : " + menu.getMeal().getName();
    }

    @Override
    public void setAsText(String id) throws IllegalArgumentException {
        if (StringUtils.isEmpty(id)) {
            setValue(null);
        } else {
            Menu menu = menuService.findById(Integer.parseInt(id));
            setValue(menu);
        }
    }
}
