package net.therap.meal.system.service;

import net.therap.meal.system.entity.Day;
import net.therap.meal.system.dao.DayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author altamas.haque
 * @since 3/10/20
 */
@Service
public class DayService {

    @Autowired
    private DayDao dayDao;

    public void add(Day day) {
        dayDao.add(day);
    }

    public void update(Integer idBefore, Day dayAfter) {
        Day dayBefore = dayDao.findById(idBefore);
        dayBefore.setName(dayAfter.getName());
        dayDao.update(dayBefore);
    }

    public List<Day> findAll() {
        List<Day> days = dayDao.findAll();
        return days;
    }

    public Day findById(Integer id) {
        Day day = dayDao.findById(id);
        return day;
    }

    public void delete(Integer id) {
        dayDao.delete(id);
    }

    public boolean exists() {
        return false;
    }
}
