package net.therap.meal.system.service;

import net.therap.meal.system.dao.MealDao;
import net.therap.meal.system.entity.Item;
import net.therap.meal.system.dao.ItemDao;
import net.therap.meal.system.entity.Meal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author altamas.haque
 * @since 3/10/20
 */
@Service
public class ItemService {

    @Autowired
    private ItemDao itemDao;

    public void add(Item item) {
        itemDao.add(item);
    }

    public void update(Item itemBefore, Item itemAfter) {
        itemBefore.setName(itemAfter.getName());
        itemDao.update(itemBefore);
    }

    public List<Item> findAll() {
        List<Item> items = itemDao.findAll();
        return items;
    }

    public Item findById(Integer id) {
        Item item = itemDao.findById(id);
        return item;
    }

    @Transactional
    public void delete(Item item) {
        item = findById(item.getId());
        itemDao.delete(item);
    }

    public boolean exists(Item item) {
        boolean exists = false;
        for (Item itm : findAll()) {
            if (itm.getName().equals(item.getName())) {
                exists = true;
            }
        }
        return exists;
    }
}