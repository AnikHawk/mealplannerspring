package net.therap.meal.system.service;

import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.dao.MealDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author altamas.haque
 * @since 3/10/20
 */
@Service
public class MealService {

    @Autowired
    private MealDao mealDao;

    public void add(Meal meal) {
        mealDao.add(meal);
    }

    public void update(Meal mealBefore, Meal mealAfter) {
        mealBefore.setName(mealAfter.getName());
        mealDao.update(mealBefore);
    }

    public List<Meal> findAll() {
        List<Meal> meals = mealDao.findAll();
        return meals;
    }

    public Meal findById(Integer id) {
        Meal meal = mealDao.findById(id);
        return meal;
    }

    @Transactional
    public void delete(Meal meal) {
        meal = findById(meal.getId());
        mealDao.delete(meal);
    }

    public boolean exists(Meal meal) {
        boolean exists = false;
        for (Meal itm : findAll()) {
            if (itm.getName().equals(meal.getName())) {
                exists = true;
            }
        }
        return exists;
    }
}
