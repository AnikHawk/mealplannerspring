package net.therap.meal.system.service;

import net.therap.meal.system.entity.Day;
import net.therap.meal.system.entity.Item;
import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.entity.Menu;
import net.therap.meal.system.dao.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author altamas.haque
 * @since 2/13/20
 */
@Service
public class MenuService {

    @Autowired
    private MenuDao menuDao;

    public void add(Menu menu) {
        Menu fetchedMenu = findByDayAndMeal(menu.getDay(), menu.getMeal());
        fetchedMenu = (fetchedMenu == null) ? new Menu(menu.getDay(), menu.getMeal()) : fetchedMenu;
        fetchedMenu.setItems(menu.getItems());
        menuDao.save(fetchedMenu);
    }

    public void update(Menu menuBefore, Menu menuAfter) {
        Set<Item> items = menuAfter.getItems() == null ? new HashSet<>() : menuAfter.getItems();
        menuBefore.setItems(items);
        menuDao.save(menuBefore);
    }

    public List<Menu> findAll() {
        List<Menu> menuList = menuDao.findAll();
        return menuList;
    }

    public Menu findById(Integer id) {
        Menu menu = menuDao.findById(id);
        return menu;
    }

    public Menu findByDayAndMeal(Day day, Meal meal) {
        Menu menu = menuDao.findByDayAndMeal(day, meal);
        return menu;
    }
}
