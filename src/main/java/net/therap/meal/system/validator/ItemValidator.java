package net.therap.meal.system.validator;

import net.therap.meal.system.entity.Item;
import net.therap.meal.system.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author altamas.haque
 * @since 3/10/20
 */
@Component
public class ItemValidator implements Validator {

    @Autowired
    private ItemService itemService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Item.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Item item = (Item) target;

        if (item.getName() == null || item.getName().length() < 1) {
            errors.rejectValue("name", null, "Item name cannot be empty");
            return;
        }

        if (item.getName().length() > 45) {
            errors.rejectValue("name", null, "Item name must be between 1 and 45 characters");
            return;
        }

        if (itemService.exists(item)) {
            errors.rejectValue("name", null, item.getName() + " already exists");
            return;
        }
    }
}