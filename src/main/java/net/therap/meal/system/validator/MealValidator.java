package net.therap.meal.system.validator;

import net.therap.meal.system.entity.Meal;
import net.therap.meal.system.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author altamas.haque
 * @since 3/10/20
 */
@Component
public class MealValidator implements Validator {

    @Autowired
    private MealService mealService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Meal.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Meal meal = (Meal) target;

        if (meal.getName() == null || meal.getName().length() < 1) {
            errors.rejectValue("name", null, "Meal name cannot be empty");
            return;
        }

        if (mealService.exists(meal)) {
            errors.rejectValue("name", null, meal.getName() + " already exists");
            return;
        }
    }
}
