<html>
<head>
    <%@include file="config.jsp" %>
    <title>Save Item</title>
</head>
<body>

<%@include file="menuHeader.jsp" %>
<div align="center">
    <form:form action="${pageContext.request.contextPath}/items/add" method="post" commandName="item">
        <spring:message code="label.itemname" var="itemName"/>
        <form:input type="text" path="name" cssClass="fancyInput" placeholder="${itemName}"></form:input>
            <input type="submit" class="addButton" value="<spring:message code="label.additem"/>"><br><br>
        <form:errors path="name" cssClass="error"/>
    </form:form>
</div>

</body>
</html>
