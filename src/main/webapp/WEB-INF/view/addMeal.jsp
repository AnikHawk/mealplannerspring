<html>
<head>
    <%@include file="config.jsp" %>
    <title>Save Meal</title>
</head>
<body>

<%@include file="menuHeader.jsp" %>
<div align="center">
    <form:form action="${pageContext.request.contextPath}/meals/add" method="post" commandName="meal">
        <spring:message code="label.mealname" var="mealName"/>
        <form:input type="text" path="name" cssClass="fancyInput" placeholder="${mealName}"></form:input>
        <input type="submit" class="addButton" value="<spring:message code="label.addmeal"/>"><br><br>
        <form:errors path="name" cssClass="error"/>
    </form:form>
</div>

</body>
</html>
