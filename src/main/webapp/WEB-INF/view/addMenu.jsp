<%@include file="config.jsp" %>
<html>
<head>
    <title>Save Item in Menu</title>
</head>
<body>
<%@include file="menuHeader.jsp" %>
<div align="center">
    <form:form action="${pageContext.request.contextPath}/menu/add" method="post" modelAttribute="menu">
        <input type="hidden" name="actionType" value="save">

        <div style="width: fit-content; display: flex; flex-direction: column; justify-content: center;">
            <form:select id="days" path="day">
                <form:options items="${dayList}" itemValue="id" itemLabel="name"/>
            </form:select>

            <form:select id="meals" path="meal">
                <form:options items="${mealList}" itemValue="id" itemLabel="name"/>
            </form:select>

            <form:checkboxes items="${itemList}" path="items" itemLabel="name" itemValue="id"/>
        </div>
        <br><br>
        <input type="submit" class="addButton" value="<spring:message code="label.add"/>">

    </form:form>
</div>

</body>
</html>
