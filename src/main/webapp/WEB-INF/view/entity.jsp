<%--
  Created by altamas.haque on 3/8/20.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title></title>
</head>
<body>


<table border="1px">
    <th>Name</th>
    <th>Role</th>
    <th>Update</th>
    <th>Delete</th>

    <c:forEach items="${users}" var="user">

        <tr>
            <form:form action="user/update/${user.id}" commandName="user">
                <td>
                    <form:input path="name" value="${user.name}"></form:input>
                </td>

                <td>
                    <form:input path="role" value="${user.role}"></form:input>
                </td>

                <td>
                    <input type="submit" value="Update">
                </td>

            </form:form>


            <td>
                <form:form action="${pageContext.request.contextPath}/user/delete/${user.id}">
                    <input type="submit" value="Delete">
                </form:form>

            </td>
        </tr>

    </c:forEach>
</table>

<br><br>

<form:form action="${pageContext.request.contextPath}/user" commandName="user">
    <form:input type="text" path="name" name="name" placeholder="User name"></form:input>
    <form:input type="text" path="role" name="role" placeholder="User role"></form:input>
    <input type="submit" value="add">
</form:form>


</body>
</html>
