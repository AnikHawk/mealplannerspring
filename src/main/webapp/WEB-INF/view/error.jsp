<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", -1);
%>

<%@include file="config.jsp" %>
<html>
<head>
    <title>Home</title>
</head>
<body>

<%@include file="menuHeader.jsp" %>

<div style="display: flex; align-items: center; justify-content: center">
    <div class="error">
        <spring:message code="prompt.invalidid"/>
    </div>
</div>

</body>
</html>
