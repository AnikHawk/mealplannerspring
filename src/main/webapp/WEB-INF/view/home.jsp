<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", -1);
%>

<%@include file="config.jsp" %>
<html>
<head>
    <title>Home</title>
</head>
<body>

<%@include file="menuHeader.jsp" %>
</body>
</html>
