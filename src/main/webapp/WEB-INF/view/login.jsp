<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="config.jsp" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/> ">
    <title>Login</title>
</head>

<body>

<div align="center">
    <a href="${pageContext.request.contextPath}/home">
        <img src="<c:url value="/resources/images/therap.png"/> ">
    </a>

    <div class="home-grid">
        <form action="${pageContext.request.contextPath}/login" method="post">
            <div>
                <input type="text" class="fancyInput" name="username" placeholder="<spring:message code="prompt.username"/>">
                <input type="text" class="fancyInput" name="password" placeholder="<spring:message code="prompt.password"/>">
            </div>

            <div>
                <input type="submit" class="addButton" value="<spring:message code="label.login"/>">
            </div>
        </form>

    </div>
</div>

</body>
</html>
