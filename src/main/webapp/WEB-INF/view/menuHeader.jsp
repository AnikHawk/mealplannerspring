<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", 0);

    if (session == null || session.getAttribute("user") == null) {
        response.sendRedirect(request.getContextPath() + "/login");
    }
%>

<div align="center">
    <div class="home-header">
        <a href="${pageContext.request.contextPath}/home">
            <img src="<c:url value="/resources/images/therap.png"/> ">
        </a>

        <form action="${pageContext.request.contextPath}/logout" method="post">
            <div style="position: absolute; left: 0; top: 0">
                <a href="?lang=en" class="updateButton" style="background: darkslategrey">
                    English
                </a>
                <a href="?lang=bn" class="updateButton" style="background: darkslateblue">
                    Bengali
                </a>
            </div>

            <div style="position: absolute; right: 0; top: 0;">
                <input type="submit" value="<spring:message code="label.logout"/>" class="deleteButton">
            </div>
        </form>
    </div>


    <div class="home-grid">
        <a href="${pageContext.request.contextPath}/menu" class="fancyButton"><spring:message code="label.menu"/></a>

        <div style="display: grid; grid-template-columns: 1fr 1fr;">
            <a href="${pageContext.request.contextPath}/items" class="fancyButton">
                <spring:message code="label.items"/>
            </a>
            <a href="${pageContext.request.contextPath}/meals" class="fancyButton">
                <spring:message code="label.mealtime"/>
            </a>
        </div>

    </div>
</div>