<%@include file="config.jsp" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/> ">
    <title><spring:message code="label.items"/></title>
</head>

<body>
<%@include file="config.jsp" %>
<%@include file="menuHeader.jsp" %>

<div align="center" style="margin-top: 30px">
    <c:forEach items="${errors}" var="error">
        <div class="error">${error}</div>
    </c:forEach>
</div>

<div style="width: 50%; margin: auto; padding-top: 3%" align="center">
    <table class="fancyTable" style="margin-bottom: 5%">
        <th width="60%"><spring:message code="label.itemname"/></th>
        <th><spring:message code="label.operations"/></th>

        <c:forEach items="${itemList}" var="item">
            <tr>
                <td>
                    <c:out value="${item.name}"/>
                </td>
                <td align="center" style="display: flex; justify-content: center;">

                    <form:form action="${pageContext.request.contextPath}/items/update/${item.id}" method="get">
                        <input type="submit" class="updateButton" value="<spring:message code="label.update"/>">
                    </form:form>

                    <form:form action="${pageContext.request.contextPath}/items/delete/${item.id}" method="post">
                        <input type="submit" class="deleteButton" value="<spring:message code="label.delete"/>">
                    </form:form>

                </td>
            </tr>
        </c:forEach>
    </table>

    <form action="${pageContext.request.contextPath}/items/add" method="get">
        <input type="submit" class="addButton" value="<spring:message code="label.additem"/>">
    </form>

</div>

</body>
</html>
