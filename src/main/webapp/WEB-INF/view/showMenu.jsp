<%@include file="config.jsp" %>

<html>
<head>
    <title><spring:message code="label.menu"/></title>
</head>
<body>
<%@ include file="menuHeader.jsp" %>

<div align="center" style="margin-top: 30px">
    <c:forEach items="${errors}" var="error">
        <div class="error">${error}</div>
    </c:forEach>
</div>

<div align="center" style="padding: 3%">
    <table class="fancyTable" style="width: 70%; margin-bottom: 3%" align="center">
        <th><spring:message code="label.day"/></th>
        <th><spring:message code="label.mealtime"/></th>
        <th style="width: 50%"><spring:message code="label.items"/></th>
        <th><spring:message code="label.update"/></th>

        <c:forEach items="${menuList}" var="menu">
            <c:if test="${not empty menu.items}">
                <tr>
                    <td><c:out value="${menu.day.name}"/></td>
                    <td><c:out value="${menu.meal.name}"/></td>
                    <td style="display: flex; flex-direction: row">
                        <c:forEach items="${menu.items}" var="item">
                        <div style="display: flex; flex-direction: row;">

                            <button class="deleteButton" style="background: gray">
                                <c:out value="${item.name}"/> <br>
                            </button>

                            </c:forEach>
                    </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/menu/update/${menu.id}"
                           class="updateButton"><spring:message code="label.update"/></a>
                    </td>
                </tr>

            </c:if>
        </c:forEach>
    </table>

    <form action="${pageContext.request.contextPath}/menu/add" method="get">
        <input type="submit" class="addButton" value="<spring:message code="label.add"/>">
    </form>
</div>

</body>
</html>
