<html>
<head>
    <%@include file="config.jsp" %>
    <title>Save Item</title>
</head>
<body>
<%@include file="menuHeader.jsp" %>

<div align="center">
    <form:form action="${pageContext.request.contextPath}/items/update/${item.id}" method="post"
               commandName="item">
        <form:input type="text" path="name" value="${item.name}" cssClass="fancyInput"/>
        <input type="submit" class="updateButton" value="<spring:message code="label.updateitem"/>"><br><br>
        <form:errors path="name" cssClass="error"/>
    </form:form>
</div>

</body>
</html>
