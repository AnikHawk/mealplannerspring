<html>
<head>
    <%@include file="config.jsp" %>
    <title>Save Meal</title>
</head>
<body>
<%@include file="menuHeader.jsp" %>

<div align="center">
    <form:form action="${pageContext.request.contextPath}/meals/update/${meal.id}" method="post"
               modelAttribute="meal">
        <form:input type="text" path="name" value="${meal.name}" cssClass="fancyInput"/>
        <input type="submit" class="updateButton" value="<spring:message code="label.updatemeal"/>"><br><br>
        <form:errors path="name" cssClass="error"/>
    </form:form>
</div>

</body>
</html>
