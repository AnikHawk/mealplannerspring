<html>
<head>
    <%@include file="config.jsp" %>
    <title>Save Item in Menu</title>
</head>
<body>
<%@include file="menuHeader.jsp" %>
<div align="center">
    <div style="width: fit-content; padding: 20px;">
        <div>
            <h2>
                <c:out value="${menu.day.name}"></c:out>
            </h2>
            <h3>
                <c:out value="${menu.meal.name}"></c:out>
            </h3>
            <div>
                <form:form action="${pageContext.request.contextPath}/menu/update/${menu.id}" method="post"
                           modelAttribute="menu">

                    <form:checkboxes items="${itemList}" path="items" itemLabel="name" itemValue="id"
                                     delimiter="<br/>"/>
                    <br><br>
                    <input type="submit" class="addButton" value="<spring:message code="label.update"/>">
                </form:form>
            </div>
        </div>

    </div>
</div>

</body>
</html>
