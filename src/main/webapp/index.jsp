<%--
  Created by altamas.haque on 3/5/20.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/> ">
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <title></title>
</head>
<body>

<c:redirect url="/home"/>

</body>
</html>
